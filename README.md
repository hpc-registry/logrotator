# Logrotator sidecar container

Useful for log rotation of logs like vault's audit.

Example pod:

```yaml
apiVersion: v1
kind: Pod
metadata:
  generateName: vault-
  labels:
    app.kubernetes.io/instance: vault
    app.kubernetes.io/name: vault
    component: server
    statefulset.kubernetes.io/pod-name: vault-0
    vault-version: 1.12.3
  name: vault-0
spec:
  affinity:
    podAntiAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        - labelSelector:
            matchLabels:
              app.kubernetes.io/instance: vault
              app.kubernetes.io/name: vault
              component: server
          topologyKey: kubernetes.io/hostname
  containers:
  - image: hashicorp/vault:1.12.3
    name: vault
    env:
      - name: VAULT_CERT_DIR
        value: /vault/userconfig/tls
      # omitted
    # omitted
    volumeMounts:
      - mountPath: /vault/userconfig/tls
        name: vault-tls
        readOnly: true
      # omitted
  - name: logrotator
    image: registry.ethz.ch/hpc-registry/logrotator:latest
    command:
      - /bin/sh
      - -c
    args:
      - |
        while true; do
          logrotate \
            --verbose \
            --state /var/tmp/logrotator.state \
            /etc/logrotate/vault-audit.conf
          sleep $((24 * 60 * 60))
        done
    resources:
      requests:
        cpu: 20m
        memory: 20Mi
        ephemeral-storage: 2Mi
      limits:
        cpu: 1
        memory: 150Mi
        ephemeral-storage: 8Mi
    volumeMounts:
      - mountPath: /vault/audit
        name: audit
      - mountPath: /etc/logrotate/vault-audit.conf
        name: config-logrotator
        readOnly: true
  restartPolicy: Always
  securityContext:
    fsGroup: 1000
    runAsGroup: 1000
    runAsNonRoot: true
    runAsUser: 100
  serviceAccount: vault
  volumes:
    - name: vault-tls
      secret:
        defaultMode: 420
        secretName: vault-core-selfsigned
    - name: config-logrotator
      configMap:
        defaultMode: 420
        name: audit-logrotator
    # omitted
  # omitted
```

Together with a logroator config like:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: audit-logrotator
data:
  logrotate-audit.conf: |
    /vault/audit/audit.log {
        rotate 32
        daily
        minsize 64M
        maxsize 256M
        #Do not execute rotate if the log file is empty.
        notifempty
        missingok
        copytruncate
        extension log
        dateext
        dateformat %Y-%m-%d.
    }
```
