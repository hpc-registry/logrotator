# renovate: datasource=docker depName=docker.io/alpine
ARG IMAGE_TAG=3.20@sha256:de4fe7064d8f98419ea6b49190df1abbf43450c1702eeb864fe9ced453c1cc5f
FROM alpine:${IMAGE_TAG}

# hadolint ignore=SC2086
RUN version="$(sed -n 's/^VERSION_ID=v\?\(\d\+\.\d\+\).*/\1/p' </etc/os-release)" && \
    ( echo http://mirror-01.ethz.ch/alpine/v${version}/main; \
      echo http://mirror-01.ethz.ch/alpine/v${version}/community; \
    ) >/etc/apk/repositories

# renovate: datasource=repology depName=alpine_3_20/logrotate versioning=semver-coerced
ARG LOGROTATE_VERSION=3.21.0
# renovate: datasource=repology depName=alpine_3_20/tini versioning=semver-coerced
ARG TINI_VERSION=0.19.0

# renovate: datasource=docker depName=docker.io/alpine
ARG IMAGE_TAG=3.20@sha256:de4fe7064d8f98419ea6b49190df1abbf43450c1702eeb864fe9ced453c1cc5f
ARG CREATED=""
ARG REVISION=""

# hadolint ignore=DL3019
RUN apk update \
 && apk upgrade \
 && apk add \
        logrotate~="${LOGROTATE_VERSION}" \
        tini~="${TINI_VERSION}" \
 && rm -rf /var/cache/apk/

ENTRYPOINT ["/sbin/tini", "/usr/sbin/logrotate", "--"]

LABEL org.opencontainers.image.authors "Michal Minář <michal.minar@id.ethz.ch>"
LABEL org.opencontainers.image.source "https://gitlab.ethz.ch/hpc-registry/logrotator"
LABEL org.opencontainers.image.url="https://gitlab.ethz.ch/hpc-registry/logrotator"
LABEL org.opencontainers.image.title "Logrotator for container logs"
LABEL org.opencontainers.image.base="docker.io/alpine:${IMAGE_TAG}"
LABEL org.opencontainers.image.licenses="AGPL-3.0-or-later"
LABEL org.opencontainers.image.created="${CREATED}"
